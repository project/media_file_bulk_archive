<?php

namespace Drupal\media_file_bulk_archive\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ZipArchive;

/**
 * Provides a confirmation form before archiving files.
 */
class FileBulkArchiveConfirmForm extends ConfirmFormBase {

  /**
   * The array of files to archive.
   *
   * @var \Drupal\file\FileInterface[]
   */
  protected $files = [];

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $filesystem;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $tempStore;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a FileDeleteMultipleForm object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   The file system.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    TimeInterface $time,
    FileSystemInterface $filesystem,
    FileRepositoryInterface $file_repository,
    FileUrlGeneratorInterface $file_url_generator) {
    $this->tempStore = $temp_store_factory->get('file_bulk_archive_confirm');
    $this->time = $time;
    $this->filesystem = $filesystem;
    $this->fileRepository = $file_repository;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('datetime.time'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_file_bulk_archive_file_bulk_archive_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Confirm archival?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_config');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->files = $this->tempStore->get('file_zip_action');

    $form = parent::buildForm($form, $form_state);
    $drupalDateTime = DrupalDateTime::createFromTimestamp($this->time->getCurrentTime());

    $form['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#description' => $this->t('The filename to create.'),
      '#default_value' => 'archive_' . $drupalDateTime->format('c'),
      '#field_suffix' => '.zip',
      '#required' => TRUE,
    ];

    $form['files_markup'] = [
      '#markup' => $this->t('The following files will be added to the archive.'),
    ];

    $form['files'] = [
      '#theme' => 'item_list',
      '#items' => array_map(function (FileInterface $file) {
        return $file->label();
      }, $this->files),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->files)) {
      $this->archive($this->files, $form['filename']['#value']);
      $this->tempStore->delete('file_zip_action');
    }
    $form_state->setRedirect('entity.file.collection');
  }

  /**
   * Archives a list of files into a zip.
   *
   * @param \Drupal\file\FileInterface[] $entities
   *   The files to archive.
   * @param string $filename
   *   The archive filename to create.
   */
  private function archive(array $entities, string $filename) {
    if (!is_array($entities)) {
      $entities = [$entities];
    }

    $filename .= '.zip';
    $wrapper = 'temporary';
    $destination = $wrapper . '://' . $filename;
    $system_destination = $this->filesystem->realpath($destination);

    // We have this extra step in order to put the file in the managed table.
    $file = $this->fileRepository->writeData('', $destination, FileSystemInterface::EXISTS_REPLACE);
    // We use ZipArchive instead of ArchiveManager because the output of the
    // later one is not as expected.
    $zip = new \ZipArchive();
    $zip->open($system_destination, ZipArchive::CREATE);

    foreach ($entities as $id => $entity) {
      $file_path = $this->filesystem->realpath($entity->getFileUri());
      $zip->addFile($file_path, $entity->getFilename());
    }

    $zip->close();
    $file->setTemporary();
    $file->save();

    // Generate the link for download.
    $file_url = $this->fileUrlGenerator->generate($file->getFileUri());
    $this->messenger()->addStatus($this->t('Export file created, <a href="@link">Click here</a> to download.', [
      '@link' => $file_url->toString(),
    ]));
  }

}
