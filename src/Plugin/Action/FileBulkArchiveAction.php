<?php

namespace Drupal\media_file_bulk_archive\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides archiving of files action.
 *
 * @Action(
 *   id = "file_bulk_archive_action",
 *   label = @Translation("Generate an archive of files"),
 *   type = "file",
 *   confirm_form_route_name = "media_file_bulk_archive.file_bulk_archive_confirm",
 * )
 */
class FileBulkArchiveAction extends ActionBase implements ContainerFactoryPluginInterface {
  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $filesystem;

  /**
   * The temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Constructs a new AssignOwnerNode action.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   The file system.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temporary store system.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $filesystem, PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->filesystem = $filesystem;
    $this->tempStore = $temp_store_factory->get('file_bulk_archive_confirm');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $entities_by_id = [];
    foreach ($entities as $entity) {
      $entities_by_id[$entity->id()] = $entity;
    }
    // Just save in temp store for now, zip after confirmation.
    $this->tempStore->set('file_zip_action', $entities_by_id);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIf($object instanceof FileInterface)->andIf(AccessResult::allowedIf($object->access('update')));
    return $return_as_object ? $result : $result->isAllowed();
  }

}
