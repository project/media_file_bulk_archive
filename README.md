INTRODUCTION
------------

This module provides an action to bulk archive media and files.

This will let your users download archives of multiple selected media and files.

The module does nothing on itself, you need to make use of the action somewhere.

Currently only the File entity type is supported. Media entity type to be added
soon.

The File archiving action will require some sort of bulk form to work with
Views, such as File entity module.
